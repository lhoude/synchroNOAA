lhoude@pcbiom06:/usr/bin$ cat /usr/bin/telficsam
# !/bin/bash
# controle du fichier de la veille
fic=`date --date '2 days ago' +%Y%m%d`
fichier=`echo "$fic""_gdas0p5"`

b=`echo "/mnt/BIGDATA/PUBLIC/arlftp.arlhq.noaa.gov/pub/archives/gdas0p5/$fichier"`
taille=`stat --printf="%s" "$b"`
if [ "$taille" = "0" ]
then

a=`echo "ftp://arlftp.arlhq.noaa.gov/pub/archives/gdas0p5/$fichier"`
wget -O "/mnt/BIGDATA/PUBLIC/arlftp.arlhq.noaa.gov/pub/archives/gdas0p5/$fichier"  "$a"
fi

# telechargement du fichier du jour
fic=`date --date '1 days ago' +%Y%m%d`
fichier=`echo "$fic""_gdas0p5"`
a=`echo "ftp://arlftp.arlhq.noaa.gov/pub/archives/gdas0p5/$fichier"`
wget -O "/mnt/BIGDATA/PUBLIC/arlftp.arlhq.noaa.gov/pub/archives/gdas0p5/$fichier"  "$a"

